# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup pn=Vulkan-Loader tag=v${PV} ] \
    cmake \
    python [ blacklist=2 multibuild=false has_bin=false has_lib=false ]

SUMMARY="Vulkan Installable Client Driver (ICD) desktop loader"
DESCRIPTION="
Vulkan is an explicit API, enabling direct control over how GPUs actually work. As such, Vulkan
supports systems that have multiple GPUs, each running with a different driver, or ICD (Installable
Client Driver). Vulkan also supports multiple global contexts (instances, in Vulkan terminology).
The ICD loader is a library that is placed between a Vulkan application and any number of Vulkan
drivers, in order to support multiple drivers and the instance-level functionality that works
across these drivers. Additionally, the loader manages inserting Vulkan layer libraries, such as
validation layers, between an application and the drivers.
"
HOMEPAGE+=" https://www.khronos.org/vulkan"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    X
    wayland
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        virtual/pkg-config
        X? ( x11-libs/libXrandr )
    build+run:
        sys-libs/vulkan-headers[>=$(ever range 1-3)]
        X? (
            x11-libs/libxcb
            x11-libs/libX11
        )
        !sys-libs/vulkan [[
            description = [ The vulkan package has been split into individual packages ]
            resolution = uninstall-blocked-after
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_TESTS:BOOL=FALSE
    -DBUILD_WERROR:BOOL=FALSE
    -DBUILD_WSI_DIRECTFB_SUPPORT:BOOL=FALSE
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DCODE_COVERAGE:BOOL=FALSE
    -DLOADER_CODEGEN:BOOL=TRUE
    -DLOADER_ENABLE_ADDRESS_SANITIZER:BOOL=FALSE
    -DLOADER_ENABLE_THREAD_SANITIZER:BOOL=FALSE
    -DUPDATE_DEPS:BOOL=FALSE
    -DUSE_GAS:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    "X WSI_XCB_SUPPORT"
    "X WSI_XLIB_SUPPORT"
    "wayland WSI_WAYLAND_SUPPORT"
)

src_install() {
    cmake_src_install

    keepdir /etc/vulkan/explicit_layer.d
    keepdir /etc/vulkan/icd.d
    keepdir /etc/vulkan/implicit_layer.d
}


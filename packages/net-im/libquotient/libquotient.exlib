# Copyright 2019-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

ever is_scm && SCM_EXTERNAL_REFS="3rdparty/libQtOlm:"

require github [ user=quotient-im pn=libQuotient ] cmake
require xdummy [ phase=test ]

export_exlib_phases src_configure src_compile src_test src_install

SUMMARY="Qt5 library to write cross-platfrom clients for Matrix"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    e2ee [[ description = [ Support for end-to-end encryption ] ]]

    ( providers: qt5 qt6 ) [[ number-selected = at-least-one ]]
"

QT_MIN_VER="5.15.0"

DEPENDENCIES="
    build+run:
        e2ee? (
            dev-libs/olm[>=3.2.5]
            dev-libs/openssl:=[>=1.1.0]
            x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        )
        providers:qt5? (
            sys-auth/qtkeychain[providers:qt5]
            x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
            x11-libs/qtmultimedia:5[>=${QT_MIN_VER}]
        )
        providers:qt6? (
            sys-auth/qtkeychain[providers:qt6]
            x11-libs/qtbase:6[>=6.0]
        )
        !net-im/libqmatrixclient [[
            description = [ libqmatrixclient has been renamed to libquotient ]
            resolution = uninstall-blocked-before
        ]]
"

_libquotient_conf() {
    local cmake_params=(
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DQuotient_INSTALL_TESTS:BOOL=FALSE
        $(cmake_option e2ee Quotient_ENABLE_E2EE)
        "$@"
    )

    ecmake "${cmake_params[@]}"
}

libquotient_src_configure() {
     if option providers:qt5; then
        edo mkdir "${WORKBASE}"/qt5-build
        edo pushd "${WORKBASE}"/qt5-build
        _libquotient_conf -DBUILD_WITH_QT6:BOOL=FALSE
        popd
    fi

    if option providers:qt6; then
        edo mkdir "${WORKBASE}"/qt6-build
        edo pushd "${WORKBASE}"/qt6-build
        _libquotient_conf -DBUILD_WITH_QT6:BOOL=TRUE
        popd
    fi
}

libquotient_src_compile() {
     if option providers:qt5; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_build
        popd
    fi

    if option providers:qt6; then
        edo pushd "${WORKBASE}"/qt6-build
        ecmake_build
        popd
    fi
}

libquotient_src_test() {
    xdummy_start

    # seems to need a running home server
    local tests_to_skip="(testolmaccount"

    # Not sure why it fails
    option e2ee && tests_to_skip+="|testkeyverification"

    tests_to_skip+=")"

     if option providers:qt5; then
        edo pushd "${WORKBASE}"/qt5-build
        ectest -E ${tests_to_skip}
        popd
     fi

     if option providers:qt6; then
        edo pushd "${WORKBASE}"/qt6-build
        # testolmaccounts seems to need a running home server
        ectest -E ${tests_to_skip}
        popd
     fi

    xdummy_stop
}

libquotient_src_install() {
     if option providers:qt5; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_install
        popd
    fi

    if option providers:qt6; then
        edo pushd "${WORKBASE}"/qt6-build
        ecmake_install
        popd
    fi

    edo pushd "${CMAKE_SOURCE}"
    emagicdocs
    edo popd
}


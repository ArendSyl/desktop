# Copyright 2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2


if ever at_least 8.62 ; then
    require gitlab [ user=nicolasfella pn=${PN}d branch=qt6 tag=VERSION_${PV} suffix=tar.bz2 new_download_scheme=true ]
    require qmake [ slot=6 ]
else
    require gitlab [ user=accounts-sso pn=${PN}d tag=VERSION_${PV} suffix=tar.bz2 new_download_scheme=true ]
    require qmake
fi
require test-dbus-daemon

export_exlib_phases src_unpack src_prepare src_configure src_compile src_install

SUMMARY="A framework for centrally storing authentication credentials"
DESCRIPTION="
The SignOn daemon is a D-Bus service which performs user authentication on behalf of its clients.
There are currently authentication plugins for OAuth 1.0 and 2.0, SASL, Digest-MD5, and plain
username/password combination.
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc
"
# TODO: currently doesn't build
#    cryptsetup [[ description = [ Build the cryptsetup extension plugin ] ]]

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        sys-apps/dbus
        x11-libs/qtbase:5[>=5.5.0][sql]
"
#        cryptsetup? ( sys-fs/cryptsetup )

# Restrict the tests for now
ever at_least 8.62 && RESTRICT="test"

signon_src_unpack() {
    gitlab_src_unpack

    # signon-dbus-specification submodule
    if ever is_scm ; then
        :
    else
        edo mv "${WORKBASE}"/signon-dbus-specification-${SIGNON_DBUS_SPECIFICATION_REV}-${SIGNON_DBUS_SPECIFICATION_REV}/* \
            "${WORK}"/lib/signond/interfaces
    fi
}

signon_src_prepare() {
    default

    # qdbusxml2cpp works with both Qts but if only Qt5 is present the path
    # isn't found, because it's installed in /usr/${host}/lib/qt5/bin
    if ever at_least 8.62 ; then
        edo sed -e "s%qdbusxml2cpp%/usr/$(exhost --target)/lib/qt6/bin/qdbusxml2cpp%" \
                -i src/signond/signond.pro
    else
        edo sed -e "s%qdbusxml2cpp%/usr/$(exhost --target)/lib/qt5/bin/qdbusxml2cpp%" \
                -i src/signond/signond.pro
    fi

    # Fix install location of dbus services files
    edo sed -e "/dbus_files.path/s:=.*\$\${INSTALL_PREFIX}:=/usr:" \
            -i lib/signond/signond.pro
    edo sed -e "/service.path/s:=.*\$\${INSTALL_PREFIX}:=/usr:" \
            -i server/server.pro
}

signon_src_configure() {
    local qmake_params=(
        PREFIX=/usr/$(exhost --target)
        LIBDIR=/usr/$(exhost --target)/lib
        CONFIG+=enable-p2p
    )
        #option cryptsetup && config+=" CONFIG+=cryptsetup"

    option doc || edo sed -e "/include(.*doc\/doc.pri.*)/d" \
        -i signon.pro lib/plugins/plugins.pro lib/SignOn/SignOn.pro

    if ever at_least 8.62 ; then
        eqmake 6 "${qmake_params[@]}"
    else
        eqmake 5 "${qmake_params[@]}"
    fi
}

signon_src_compile() {
    default

    option doc && emake docs
}

signon_src_install() {
    default

    edo mv "${IMAGE}"/usr/$(exhost --target)/share/doc/* \
           "${IMAGE}"/usr/share/doc/${PNVR}
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share/{doc,}
}


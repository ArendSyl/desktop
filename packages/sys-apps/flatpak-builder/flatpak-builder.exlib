# Copyright 2017-2018 Rasmus thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ suffix=tar.xz release=${PV} user=flatpak ]
require meson

export_exlib_phases src_prepare src_test

SUMMARY="Tool to build flatpaks from source"
HOMEPAGE="https://flatpak.org/"

# TODO: Remove with next sane tarball, cf. https://github.com/flatpak/flatpak-builder/issues/590
DOWNLOADS="https://github.com/flatpak/flatpak-builder/releases/download/1.4.2/flatpak-builder-1.4.2-fixed-libglnx.tar.xz"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc
    ( providers: elfutils libelf ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18.2]
        virtual/pkg-config[>=0.24]
        doc? (
            app-text/docbook-xml-dtd:4.3
            app-text/docbook-xsl-stylesheets
            app-text/xmlto
            dev-libs/libxslt
        )
    build+run:
        core/json-glib
        dev-libs/glib:2[>=2.66]
        dev-libs/appstream[>=0.15.0][compose]
        dev-libs/libxml2:2.0[>=2.4]
        dev-libs/libyaml
        dev-util/debugedit[>=5.0]
        net-misc/curl
        sys-apps/flatpak[>=0.99.1]
        sys-devel/libostree:1[>=2017.14]
        providers:elfutils? ( dev-util/elfutils )
        providers:libelf? ( dev-libs/libelf[>=0.8.12] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    # Optimizations for libostree build again fuse:3
    -Dfuse=3
    -Dyaml=enabled
    -Dinstalled_tests=false
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'doc docs'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

# test-libgnlx-fdio fails because of sydbox
RESTRICT="test"

flatpak-builder_src_prepare() {
    meson_src_prepare

    # Don't use /var/tmp as test dir, but ${TEMP}
    edo sed -e "s:/var/tmp/test-flatpak-XXXXXX:"${TEMP%/}"/test-flatpak-XXXXXX:" -i \
        tests/libtest.sh

    # Insert rant about meson missing docdir
    edo sed -e "/doc_dir = /s/meson.project_name()/'${PNVR}'/" \
        -i doc/meson.build
}

flatpak-builder_src_test() {
    esandbox allow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent"
    esandbox allow_net --connect "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent"
    esandbox allow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent.*"

    meson_src_test

    esandbox disallow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent.*"
    esandbox disallow_net --connect "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent"
    esandbox disallow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent"
}


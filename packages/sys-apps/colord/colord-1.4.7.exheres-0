# Copyright 2011 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2013-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson
require bash-completion
require gsettings
require test-dbus-daemon
require vala [ vala_dep=true with_opt=true option_name=vapi ]

SUMMARY="Color management system daemon"
DESCRIPTION="
colord is a system service that makes it easy to manage, install and generate color profiles to
accurately color manage input and output devices.
"
HOMEPAGE="https://www.freedesktop.org/software/${PN}"
DOWNLOADS="${HOMEPAGE}/releases/${PNV}.tar.xz"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk-doc
    print-profiles [[ description = [ Build and install basic printing ICC profiles ] ]]
    sane [[ description = [ Support for Scanner Access Now Easy ] ]]
    sensors [[ description = [ Support calibration devices missing a native sensor driver via ArgyllCMS ] ]]
    systemd
    vapi
"

# requires X
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.17]
        virtual/pkg-config[>=0.23]
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
        print-profiles? ( measurement/argyllcms )
    build+run:
        group/colord
        user/colord
        dev-db/sqlite:3
        dev-libs/glib:2[>=2.58.0]
        dev-libs/libgusb[>=0.3.0][gobject-introspection]
        gnome-desktop/gobject-introspection:1[>=0.9.8]
        gnome-desktop/libgudev
        media-libs/lcms2[>=2.6]
        sys-auth/polkit:1[>=0.103]
        sane? (
            media-gfx/sane-backends
            sys-apps/dbus
        )
        sensors? ( measurement/argyllcms )
        systemd? ( sys-apps/systemd )
    suggestion:
        gnome-desktop/gnome-color-manager [[
            description = [ GNOME color profile manager and calibration tool ]
        ]]
        kde/colord-kde:4 [[
            description = [ KDE color profile manager KCM module ]
        ]]
"
MESON_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    -Dbash_completion=false
    -Ddaemon=true
    -Ddaemon_user=colord
    -Dinstalled_tests=false
    -Dintrospection=true
    -Dlibcolordcompat=true
    -Dman=false
    -Dpnp_ids=/usr/share/misc/pnp.ids
    -Dsession_example=false
    -Dtests=false
    -Dudev_rules=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc docs'
    'print-profiles print_profiles'
    sane
    'sensors argyllcms_sensor'
    systemd
    vapi
)

src_install() {
    meson_src_install

    keepdir /var/lib/colord{,/icc}
    edo chmod 0755 "${IMAGE}"/var/lib/colord{,/icc}
    edo chown colord:colord "${IMAGE}"/var/lib/colord{,/icc}

    dobashcompletion data/colormgr colormgr
}

